# This is repository for home projects in OTUS course

## List of projects available:
- Ветвления и управление потоком выполнения приложения. Циклы (m1p4)
- Коллекции. Операторы управления коллекциями: map, flatMap, compactMap, reduce (m1p5)
- Функции, методы (m1p6)
- Наследование, переопределение методов родительского класса (m2p11)
- Дженерики и продвинутые операторы (m2p13)
- Продвинутые функции и замыкания (m2p14)
- Введение в UIKit, Interface Builder Basics, XIB, Storyaboard, Autolayout. Создаем простые экраны. Верстка UI с помощью кода, NSLayoutConstraints (m2p16)
- Навигация: Navigation Controllers, Segues, Tab Bar Controllers (m2p17)
- Отображение списочных данных:Table View (m2p18)
- Отображение списочных данных:Collection View (m2p19)
- Архитектурные паттерны iOS: MVC, MVP, MVVM, Clean(VIPER) (m2p20)
- Архитектурные паттерны iOS и SOLID, GoF (m2p21)
- Многопоточность. GCD, Async/await (m2p23)
- Работаем с сетью: HTTP и URLSession, JSON, Разбираем работу с сетью с готовыми библиотеками и решениям: Alamofire, Moya
- Core Data #1 (m2p25)
- User Defaults, работа с файловой системой (m4p26)
- Unit тесты (m4p27)
- Анимация: CoreAnimation, custom transition (m3p22)

## Upcoming:

- SPM, Cocoapods. Управляем зависимостями, создаем свои библиотеки, настраиваем многомодульное приложение

