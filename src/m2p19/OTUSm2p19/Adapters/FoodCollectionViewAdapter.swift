//
//  MainCollectionViewAdapter.swift
//  OTUSm2p19
//
//  Created by Konstantin Tukmakov on 31.10.2022.
//

import UIKit

protocol CollectionViewAdapter {
    func setup(_ for: UICollectionView)
}

class FoodCollectionViewAdapter: NSObject, CollectionViewAdapter {
    var food = Food.mockup
    
    func setup(_ collectionView: UICollectionView) {
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(FoodCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
    }
    
}

extension FoodCollectionViewAdapter: UICollectionViewDelegate {

}

extension FoodCollectionViewAdapter: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return food.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FoodCollectionViewCell
        cell.food = food[indexPath.item]
        return cell
    }
    
}



extension FoodCollectionViewAdapter: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       print(collectionView.frame.width)
       return CGSize(width: collectionView.frame.width * 0.5 - 20, height: collectionView.frame.height * 0.5)
   }
       
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
       return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
   }
}
