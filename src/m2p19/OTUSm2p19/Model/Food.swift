//
//  File.swift
//  OTUSm2p19
//
//  Created by Konstantin Tukmakov on 30.10.2022.
//

import Foundation

struct Food {
    var title: String
    var presentationImgPath: String
}

extension Food {
    static let mockup: [Food] = {
        return [
            Food(title: "Burger", presentationImgPath: "burger"),
            Food(title: "Pasta", presentationImgPath: "pasta"),
            Food(title: "Borsch", presentationImgPath: "borsch"),
            Food(title: "Hot Dog", presentationImgPath: "hotdog"),
            Food(title: "Cinnabon", presentationImgPath: "cinnabon")
        ]
    }()
}
