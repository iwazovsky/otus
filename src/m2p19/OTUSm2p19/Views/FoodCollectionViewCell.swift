//
//  MainCollectionViewCell.swift
//  OTUSm2p19
//
//  Created by Konstantin Tukmakov on 30.10.2022.
//

import UIKit

class FoodCollectionViewCell: UICollectionViewCell {
    var food: Food? {
        didSet {
            guard let food = food else { return }
            presentationImageView.image = UIImage(named: food.presentationImgPath)
            titleLabel.text = food.title
        }
    }
    
    let presentationImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.layer.cornerRadius = 13
        view.clipsToBounds = true
        return view
    }()
    
    let titleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
        initConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initViews() {
        self.contentView.addSubview(presentationImageView)
        self.contentView.addSubview(titleLabel)
    }
    
    func initConstraints() {
        NSLayoutConstraint.activate([
            presentationImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            presentationImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            presentationImageView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            presentationImageView.heightAnchor.constraint(equalToConstant: contentView.frame.height * 0.75),

            titleLabel.topAnchor.constraint(equalTo: presentationImageView.bottomAnchor, constant: 20),
            titleLabel.centerXAnchor.constraint(equalTo: presentationImageView.centerXAnchor)
        ])
    }
}
