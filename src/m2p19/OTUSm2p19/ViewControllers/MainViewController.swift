//
//  ViewController.swift
//  OTUSm2p19
//
//  Created by Konstantin Tukmakov on 30.10.2022.
//

import UIKit

class MainViewController: UIViewController {
    
    let mainCollectionViewAdapter = FoodCollectionViewAdapter()
    
    lazy var collectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        return layout
    }()
    
    lazy var collectionView: UICollectionView = {
        let view = UICollectionView(frame: CGRectZero, collectionViewLayout: self.collectionViewLayout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.showsVerticalScrollIndicator = false
        mainCollectionViewAdapter.setup(view)
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        initConstraints()
    }

    func initViews() {
        self.view.backgroundColor = .white
        self.view.addSubview(collectionView)
    }
    
    func initConstraints() {
        let safeArea = self.view.safeAreaLayoutGuide
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: safeArea.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor)
        ])
    }
}
