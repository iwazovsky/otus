//
//  Error.swift
//  m2p24
//
//  Created by Konstantin Tukmakov on 29.11.2022.
//

import Foundation

enum CustomError: Error {
    case runtimeError(String)
}
