//
//  ICoordinator.swift
//  OTUSm2p21
//
//  Created by Konstantin Tukmakov on 13.11.2022.
//

import UIKit

protocol ICoordinator: AnyObject {
    var children: [ICoordinator] { get }
    var navigationController: UINavigationController? { get set }
    
    func start()
    
    init(navigationController: UINavigationController)
}
