//
//  StockDetailViewController.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 08.11.m2022.
//

import UIKit

class StockDetailViewController: UIViewController {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var viewModel: StockDetailViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        viewModel.setup()
        
        initViews()
        initConstraints()
    }
    
    private func initViews() {
        view.addSubview(titleLabel)
        view.addSubview(valueLabel)
    }
    
    private func initConstraints() {
        let safeArea = view.safeAreaLayoutGuide
        
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: safeArea.centerYAnchor),
            valueLabel.centerXAnchor.constraint(equalTo: titleLabel.centerXAnchor),
            valueLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
        ])
    }

}

extension StockDetailViewController: ViewModelDelegate {
    func willLoadData() {
        
    }
    
    func didLoadData() {
        titleLabel.text = viewModel.stock?.title
        
        guard let stockValue = viewModel.stock?.value else { return }
        valueLabel.text = String(format: "%.2f", stockValue)
    }
}
