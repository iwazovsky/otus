//
//  StockService.swift
//  OTUSm2p21
//
//  Created by Konstantin Tukmakov on 12.11.2022.
//

import Foundation

class StockService {
    static let shared = StockService()
    
    private init() {}
    
    func fetchStocks() -> [Stock] {
        return [
            Stock(symbol: "AAPL", title: "Apple", value: 0),
            Stock(symbol: "GOOG", title: "Google", value: 0),
            Stock(symbol: "AMZN", title: "Amazon", value: 0)
        ]
    }
    
    func fetchPrice(for stock: Stock, completion: @escaping((_ price: Double) -> Void)) {
        let client = AlphavantageApiClient()
        client.request(path: "query?function=GLOBAL_QUOTE&symbol=\(stock.symbol)", method: .get) { (result: Result<AlphavantageQuoteEndpointResponse?, Error>) in
            switch result {
            case .success(let data):
                completion(data?.globalQuote.price ?? 0)
            case .failure(let err):
                print(err)
            }
        }
    }
}
