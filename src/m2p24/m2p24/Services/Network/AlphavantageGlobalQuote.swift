//
//  AlphavantageGlobalQuote.swift
//  m2p24
//
//  Created by Konstantin Tukmakov on 28.11.2022.
//

import Foundation

struct AlphavantageQuoteEndpointResponse: Decodable {
    let globalQuote: GlobalQuote
    
    enum CodingKeys: String, CodingKey {
        case globalQuote = "Global Quote"
    }

    struct GlobalQuote: Decodable {
        let symbol: String
        let price: Double
        
        enum CodingKeys: String, CodingKey {
            case symbol = "01. symbol"
            case price = "05. price"
        }
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.symbol = try container.decode(String.self, forKey: .symbol)
            
            guard let price = Double(try container.decode(String.self, forKey: .price)) else {
                throw CustomError.runtimeError("Price cannot be parse as float")
            }
                                    
            self.price = price
        }
    }
}
