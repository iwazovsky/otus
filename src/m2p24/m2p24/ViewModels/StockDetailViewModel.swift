//
//  StockDetailViewModel.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 08.11.2022.
//

import Foundation

class StockDetailViewModel: ViewModel {
    weak var delegate: ViewModelDelegate?
    var stock: Stock?
    var stockService: StockService!
    
    
    func setup() {
        
        DispatchQueue.main.async {
            guard self.stock != nil else { return }
            
            self.stockService.fetchPrice(for: self.stock!) { price in
                self.stock?.value = price
                self.delegate?.didLoadData()
            }
        }
        
        delegate?.didLoadData()
    }
}
