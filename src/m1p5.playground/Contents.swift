/*
1. Есть произвольный массив чисел, найти максимальное и минимальное число и поменять их местами
*/

func swapMinMax(_ numbers: inout [Int]) {
    guard numbers.count > 0 else {
        return
    }
    
    var min: (index: Int, value: Int) = (-1, Int.max)
    var max: (index: Int, value: Int) = (-1, Int.min)
    
    for (i, k) in numbers.enumerated() {
        if k < min.value {
            min = (i, k)
        }
        if k > max.value {
            max = (i, k)
        }
    }
    
    numbers.swapAt(min.index, max.index)
}

// test invocation
//var random = [1, 2, 4121, -1, -12, 232, 8138]
//swapMinMax(&random)
//print(random)

/*
2. Есть два массива символов - собрать результирующее множество из символов, что повторяются в 2х массивах
 */

func intersection(_ a: [Character], _ b: [Character]) -> Set<Character> {
    var intersected = Set<Character>()
    
    for char in a {
        if b.contains(char) {
            intersected.insert(char)
        }
    }
    
    return intersected
}

// test invocation
//var chars1: [Character] = ["a", "b", "c", "d"]
//var chars2: [Character] = ["b", "d"]
//
//print(intersection(chars1, chars2))


/*
 3. Создать словарь с соотношением имя (ключ) пользователя - пароль (значение), получить из словаря все имена, пароли которых длиннее 10 символов
 Задачи рекомендуется делать в Playground
 */

var users: [String: String] = [
    "Mike Wazovky": "123abc",
    "Andrew Garcia": "abc123",
    "Bill Gates": "AnyStrongPassword1291*",
    "Tom Morello": "IHave15GuitarsInMyPocket",
    "Joker": "99-1Bat"
]

//print(users.filter { _, password in
//    password.count > 10
//})
