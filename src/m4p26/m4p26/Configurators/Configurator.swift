//
//  Configurator.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 12.11.2022.
//

import UIKit

protocol IConfigurator {
    func configure(viewController: UIViewController, coordinator: ICoordinator?)
}
