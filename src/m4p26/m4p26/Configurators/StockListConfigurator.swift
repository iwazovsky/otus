//
//  Configurator.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 12.11.2022.
//

import UIKit

class StockListConfigurator: IConfigurator {
    func configure(viewController: UIViewController, coordinator: ICoordinator?) {
        
        guard let viewController = viewController as? StockListViewController else { return }

        let viewModel = StockListViewModel()
        viewModel.stockService = StockService.shared
        viewModel.delegate = viewController
        viewController.viewModel = viewModel
        
        let tableViewAdapter = StockListTableViewAdapter()
        
        guard let tableViewCoordinator = coordinator as? StockListViewTableAdapterCoordinator else { return }
        tableViewAdapter.coordinator = tableViewCoordinator
        viewController.tableViewAdapter = tableViewAdapter
    }
}
