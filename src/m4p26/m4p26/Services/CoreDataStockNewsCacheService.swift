//
//  StockNewsCacheService.swift
//  m2p25
//
//  Created by Konstantin Tukmakov on 12.12.2022.
//

import Foundation

class CoreDataStockNewsCacheService: StockNewsCacheProvider {
    
    static let shared = CoreDataStockNewsCacheService(coreDataManager: CoreDataManager.shared)
    let coreDataManager: CoreDataManager
    
    init(coreDataManager: CoreDataManager) {
        self.coreDataManager = coreDataManager
    }
    
    func storeNewsItems(_ items: [StockNewsItem]) {
        for item in items {
            storeNewsItem(item)
        }
    }
    
    func storeNewsItem(_ item: StockNewsItem) {
        coreDataManager.createStockNewsItem(item) { result in
            
        }
    }
    
    func fetchNewsItems(for stock: Stock, filterBy: String?, completion: @escaping([StockNewsItem]?) -> Void) {
        
        var predicate = NSPredicate(format: "symbol == %@", stock.symbol)
    
        if filterBy != nil && !filterBy!.isEmpty {
            let filterPredicate = NSPredicate(format: "title CONTAINS[c] %@", filterBy!)
            
            predicate = NSCompoundPredicate(type: .and, subpredicates: [predicate, filterPredicate])
        }

        DispatchQueue.main.async {
            self.coreDataManager.getStockNewsItems(predicate: predicate) { result in
                switch result {
                case .success(let rows):
                    let items = rows.map {
                        StockNewsItem(symbol: $0.symbol ?? "", title: $0.title ?? "", description: $0.content ?? "", image: $0.image)
                    }
                    completion(items)
                case .failure(let error):
                    print(error.localizedDescription)
                }

            }
        }
    }
}
