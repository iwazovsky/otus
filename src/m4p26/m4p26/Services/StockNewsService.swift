//
//  StockNewsService.swift
//  m2p25
//
//  Created by Konstantin Tukmakov on 11.12.2022.
//

import Foundation



protocol StockNewsProvider {
    func fetchStockNewsItems(for stock: Stock, completion: @escaping(_ items: [StockNewsItem]?) -> Void)
    func filterResults(for stock: Stock, filterBy: String, completion: @escaping(_ items: [StockNewsItem]?) -> Void)
}

protocol StockNewsCacheProvider {
    func storeNewsItem(_ item: StockNewsItem)
    func storeNewsItems(_ items: [StockNewsItem])
    func fetchNewsItems(for stock: Stock, filterBy: String?, completion: @escaping([StockNewsItem]?) -> Void)
}



class StockNewsService: StockNewsProvider {
    static let shared = StockNewsService()
    
//    var stockNewsCacheService: StockNewsCacheProvider = CoreDataStockNewsCacheService.shared
    var stockNewsCacheService: StockNewsCacheProvider = UserDefaultsStockNewsCacheService.shared
    
    private init() {}
    
    func fetchStockNewsItems(for stock: Stock, completion: @escaping(_ items: [StockNewsItem]?) -> Void) {
        
        DispatchQueue.main.async {
            
            self.stockNewsCacheService.fetchNewsItems(for: stock, filterBy: nil) { fetchedItems in
                guard fetchedItems != nil && fetchedItems?.isEmpty != true else {
                    let client = MarketauxApiClient()
                    let parameters: [String: String] = [
                        "symbols": stock.symbol,
                    ]
                    
                    client.request(path: "v1/news/all", method: .get, parameters: parameters) { [weak self] (result: Result<MarketauxNewsFetchResponse?, Error>) in
                        
                        switch result {
                        case .success(let response):
                            DispatchQueue.main.async {
                                
                                let stockNewsItems = response?.data.map {
                                    let imageData = client.fetchImage(url: $0.imageUrl)
                                    
                                    return StockNewsItem(symbol: stock.symbol, title: $0.title, description: $0.description, image: imageData)
                                }
                                
                                DispatchQueue.global().async {
                                    self?.stockNewsCacheService.storeNewsItems(stockNewsItems ?? [])
                                }
                                
                                completion(stockNewsItems)
                            }
                            
                            
                            
                        case .failure(let error):
                            print(error)
                        }
                    }
                
                    return
                }
                
                completion(fetchedItems)
            }
        }
    }
    
    func filterResults(for stock: Stock, filterBy: String, completion: @escaping(_ items: [StockNewsItem]?) -> Void) {
        DispatchQueue.main.async {
            self.stockNewsCacheService.fetchNewsItems(for: stock, filterBy: filterBy) { [weak self] newsItems in
                completion(newsItems)
            }
        }
    }
}
