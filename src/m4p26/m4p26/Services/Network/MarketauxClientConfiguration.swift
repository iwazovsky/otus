//
//  MarketauxClientConfiguration.swift
//  m2p25
//
//  Created by Konstantin Tukmakov on 12.12.2022.
//

import Foundation

struct MarketauxNetworkConfiguration {
    
    private let apiUrl = "https://api.marketaux.com/"
    private let apiKey = "9ADNvyqD4aq4IiHH7fstXzAquxsHdUxfVXC2ML5K"
    
    func getHeaders() -> [String: String] {
        return [:]
    }
    
    func getBaseUrl() -> String {
        return apiUrl
    }
    
    func getApiKey() -> String {
        return apiKey
    }
}
