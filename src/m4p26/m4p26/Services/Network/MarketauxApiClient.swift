//
//  MarketauxApiClient.swift
//  m2p25
//
//  Created by Konstantin Tukmakov on 12.12.2022.
//

import Foundation

enum MarketauxApiError: Error {
    case urlError
}

class MarketauxApiClient {
    private let configuration = MarketauxNetworkConfiguration()
    
    private let urlSessionConfiguration = URLSessionConfiguration.default
    
    private lazy var urlSession: URLSession? = {
        let urlSession = URLSession.init(configuration: urlSessionConfiguration)
        return urlSession
    }()
    
    private var dataTask: URLSessionDataTask? = nil
    
    func request<T: Decodable>(path: String, method: HttpClientMethod, parameters: [String:String] = [:], completion: @escaping(Result<T?, Error>) -> Void) {
        let apiPath = "\(configuration.getBaseUrl())\(path)"
        var url: URL?
        
        var queryItems: [URLQueryItem] = [
            URLQueryItem(name: "api_token", value: configuration.getApiKey())
        ]
        
        if !parameters.isEmpty {
            var urlComponents = URLComponents(string: "\(configuration.getBaseUrl())\(path)")
            for parameter in parameters {
                print(parameter.key, parameter.value)
                queryItems.append(URLQueryItem(name: parameter.key, value: parameter.value))
            }
            urlComponents?.queryItems = queryItems
            url = urlComponents?.url
        } else {
            url = URL(string: apiPath)
        }
        guard (url != nil) else { return }
        
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = method.value
        
        let headers = configuration.getHeaders()
        for header in headers {
            urlRequest.setValue(header.value, forHTTPHeaderField: header.key)
        }
        
        dataTask = urlSession?.dataTask(with: urlRequest, completionHandler: { data, urlResponse, error in
            if let error = error {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
            
            if let data = data {
                let content = JsonHelper.shared.decode(data: data, T.self)
                DispatchQueue.main.async {
                    completion(.success(content))
                }
            }
        })
        
        dataTask?.resume()
    }

    func fetchImage(url rawUrl: String) -> Data? {
        guard let url = URL(string: rawUrl) else {
            return nil
        }
//
//        URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
//            completion(.success(data))
//        }
        
        if let data = try? Data(contentsOf: url) {
            return data
        }
        
        return nil
    }
    
}
