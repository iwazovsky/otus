//
//  MarketauxNewsResponse.swift
//  m2p25
//
//  Created by Konstantin Tukmakov on 12.12.2022.
//

import Foundation

struct MarketauxNewsFetchResponse: Decodable {
    let data: [Item]
    let meta: Meta
    
    struct Item: Decodable {
        let uuid: String
        let title: String
        let description: String
        let imageUrl: String
        let publishedAt: String
            
        enum CodingKeys: String, CodingKey {
            case uuid = "uuid"
            case title = "title"
            case description = "description"
            case imageUrl = "image_url"
            case publishedAt = "published_at"
        }
    }
    
    struct Meta: Decodable {
        let found: Int
        let returned: Int
        let limit: Int
        let page: Int
    }
}
