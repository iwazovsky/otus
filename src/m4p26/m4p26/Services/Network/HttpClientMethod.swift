//
//  HttpClientMethod.swift
//  m2p24
//
//  Created by Konstantin Tukmakov on 29.11.2022.
//

import Foundation

enum HttpClientMethod: String {
    case get, post
    
    var value: String {
        return rawValue.uppercased()
    }
}
