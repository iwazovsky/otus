//
//  AlphavantageApiService.swift
//  m2p24
//
//  Created by Konstantin Tukmakov on 27.11.2022.
//

import Foundation

class AlphavantageApiClient {
    private let configuration = AlphavantageNetworkConfiguration()
    
    private let urlSessionConfiguration = URLSessionConfiguration.default
    
    private lazy var urlSession: URLSession? = {
        let urlSession = URLSession.init(configuration: urlSessionConfiguration)
        
        return urlSession
    }()
    
    private var dataTask: URLSessionDataTask? = nil
    
    func request<T: Decodable>(path: String, method: HttpClientMethod, parameters: [String:String] = [:], completion: @escaping(Result<T?, Error>) -> Void) {
        let apiPath = "\(configuration.getBaseUrl())\(path)&apikey=\(configuration.getApiKey())"
        guard let url = URL(string: apiPath) else { return }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.value
        
        let headers = configuration.getHeaders()
        for header in headers {
            urlRequest.setValue(header.value, forHTTPHeaderField: header.key)
        }
        
        dataTask = urlSession?.dataTask(with: urlRequest, completionHandler: { data, urlResponse, error in
            if let error = error {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
            
            if let response = urlResponse as? HTTPURLResponse {
                if response.statusCode >= 200 && response.statusCode < 400 {
                    
                }
            }
            
            if let data = data {
                let content = JsonHelper.shared.decode(data: data, T.self)
                DispatchQueue.main.async {
                    completion(.success(content))
                }
            }
        })
        
        dataTask?.resume()
        
        
    }
}
