//
//  UserDefaultsStockNewsCacheService.swift
//  m4p26
//
//  Created by Konstantin Tukmakov on 13.12.2022.
//

import Foundation

class UserDefaultsStockNewsCacheService: StockNewsCacheProvider {
    
    static let shared = UserDefaultsStockNewsCacheService()
    
    lazy var encoder: JSONEncoder = {
        return JSONEncoder()
    }()
    
    lazy var decoder: JSONDecoder = {
        return JSONDecoder()
    }()
    
    func storeNewsItem(_ item: StockNewsItem) {
        
    }
    
    func storeNewsItems(_ items: [StockNewsItem]) {
        if let encoded = try? self.encoder.encode(items) {
            UserDefaults.standard.set(encoded, forKey: "newsItems")
        }
    }
    
    func fetchNewsItems(for stock: Stock, filterBy: String?, completion: @escaping([StockNewsItem]?) -> Void) {
        
        guard let objects = UserDefaults.standard.value(forKey: "newsItems") as? Data else {
            completion(nil)
            return
        }
        guard let decoded = try? decoder.decode(Array<StockNewsItem>.self, from: objects) as? [StockNewsItem] else {
            completion(nil)
            return
        }
        
        let filtered = decoded.filter {
            if $0.symbol != stock.symbol {
                return false
            }
            if let filterBy = filterBy {
                if !filterBy.isEmpty {
                    return $0.title.lowercased().contains(filterBy ?? "")
                }
            }
            return true
        }
        completion(filtered)
        
    }
}
