

/*
 1.функция складывает две целочисленных переменных - отдает на выходе сумму
 */
func sum(_ a: Int, _ b: Int) -> Int {
    return a + b
}

//print(sum(4, 6))

/*
 2. функция принимает кортеж из числа и строки приводит число к строке и ввыводит в консоль резуультат
 */
func convertAndPrintTuple(_ a: (Int, String)) {
    print(String(a.0))
}
//convertAndPrintTuple((4, "Just a string"))

/*
 3. функция принимает на вход опциональное замыкание и целое число, выполняет замыкание только. в случае если число больше 0
 */
func doSomething(b: Int, callback: () -> Void) {
    if b > 0 {
        callback()
    }
}
//doSomething(b: 4) {
//    print("I am executed")
//}

/*
 4. функция принимает число на вход (год), проверить високосный ли он
 */
func isLeapYear(_ year: Int) -> Bool {
    return year % 400 == 0 || ((year % 100 != 0) && year % 4 == 0)
}

//print(isLeapYear(1995))
