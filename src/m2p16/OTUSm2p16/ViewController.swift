//
//  ViewController.swift
//  OTUSm2p16
//
//  Created by Konstantin Tukmakov on 21.10.2022.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var timerLabel: UILabel!
    
    @IBOutlet var btn: UIButton!
    
    var timer: Timer?
    var value: Double = 0.0
    var isRunning: Bool = false
    var timeFormat: String = "%02d:%02d,%02d"
    
    var btnTitleAttributes: [NSAttributedString.Key : Any] = [
        .font: UIFont(name: "VT323 Regular", size: 32.0)!,
        .foregroundColor: UIColor(named: "InversedMainTextColor")!
        
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        btn.setAttributedTitle(NSAttributedString(string: "Start".uppercased(), attributes: btnTitleAttributes), for: .normal)
        btn.backgroundColor = UIColor(named: "MainTextColor")
        timerLabel.textColor = UIColor(named: "MainTextColor")
        
        registerObservers()
    }
    
    func registerObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(onBackground), name: UIScene.didEnterBackgroundNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(onForeground), name: UIScene.willEnterForegroundNotification, object: nil)

        
    }

    @objc func onBackground() {
        stopTimer()
    }
    
    @objc func onForeground() {
        if isRunning {
            startTimer()
        }
    }

    @IBAction func btnPressed(_ sender: Any) {
        if isRunning {
            isRunning = false
            btn.setAttributedTitle(NSAttributedString(string: "Start".uppercased(), attributes: btnTitleAttributes), for: .normal)
            stopTimer()
        } else {
            isRunning = true
            btn.setAttributedTitle(NSAttributedString(string: "Stop".uppercased(), attributes: btnTitleAttributes), for: .normal)
            startTimer()
        }
    }
    
    @objc func onTimerTick() {
        value += 0.01
        
        let minutes = Int(floor(value/60))
        let seconds = Int(value.truncatingRemainder(dividingBy: 60))
        let milliseconds = Int(value.truncatingRemainder(dividingBy: 1) * 100)
        
        timerLabel.text = String(format: timeFormat, minutes, seconds, milliseconds)
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(onTimerTick), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: .common)
    }
    
    func stopTimer() {
        timer?.invalidate()
    }
}
