//
//  BlueViewController.swift
//  OTUSm2p17
//
//  Created by Konstantin Tukmakov on 28.10.2022.
//

import UIKit

class BlueViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onGetMoreWorkTap(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "BoringLifeViewController") {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("BlueViewController will appear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("BlueViewController will disappear")
    }
}
