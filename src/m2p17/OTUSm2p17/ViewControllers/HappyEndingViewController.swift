//
//  HappyEndingViewController.swift
//  OTUSm2p17
//
//  Created by Konstantin Tukmakov on 28.10.2022.
//

import UIKit

class HappyEndingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onRebornTap(_ sender: Any) {
        performSegue(withIdentifier: "rebornUnwind", sender: self)
    }
    
    @IBAction func onAnotherEndingTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("HappyEndingViewController will appear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("HappyEndingViewController will disappear")
    }
}
