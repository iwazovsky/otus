//
//  BoringLifeViewController.swift
//  OTUSm2p17
//
//  Created by Konstantin Tukmakov on 28.10.2022.
//

import UIKit

class BoringLifeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func onGetRestTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onGetMoreWorkTap(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TotalDisappearViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("BoringLifeViewController will appear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("BoringLifeViewController will disappear")
    }
}
