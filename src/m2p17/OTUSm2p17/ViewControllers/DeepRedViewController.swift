//
//  DeepRedViewController.swift
//  OTUSm2p17
//
//  Created by Konstantin Tukmakov on 28.10.2022.
//

import UIKit

class DeepRedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onPassOutTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("DeepRedViewController will appear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("DeepRedViewController will disappear")
    }
}
