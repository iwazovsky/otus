//
//  TotalDisappearViewController.swift
//  OTUSm2p17
//
//  Created by Konstantin Tukmakov on 28.10.2022.
//

import UIKit

class TotalDisappearViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    @IBAction func onRestALittleTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onStartOverTap(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("TotalDisappearViewController will appear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("TotalDisappearViewController will disappear")
    }
}
