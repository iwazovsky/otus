//
//  RedViewController.swift
//  OTUSm2p17
//
//  Created by Konstantin Tukmakov on 28.10.2022.
//

import UIKit

class RedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func unwindToRed(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("RedViewController will appear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("RedViewController will disappear")
    }
}
