///**
// Реализуйте дженерик-класс для структуры данных Stack, Deque или Queue.
// Класс должен содержать дженерик-методы согласно выбранному типу структуры данных.
// В Playground основном пространстве напишите пример с использованием созданного класса
// https://ru.wikipedia.org/wiki/Двухсторонняя_очередь
// https://ru.wikipedia.org/wiki/Стек
// https://ru.wikipedia.org/wiki/Очередь_(программирование)
// */
//




print("Testing Stack")
var stack = Stack<String>()
stack.push("Marshmellow")
stack.push("Zephyr")
stack.push("Pastilla")

for _ in 0...3 {
    print(stack.size(), stack.isEmpty(), stack.pop())
}

print("")
print("")
print("")


print("Testing Queue")

var queue = Queue<String>()

queue.enqueue("Mojave")
queue.enqueue("Sierra")

for _ in 0...2 {
    print(queue.size(), queue.isEmpty(), queue.dequeue())
}

print("")
print("")
print("")


print("Testing Deque")
var deque = Deque<Int>()

deque.append(1)
deque.append(2)
deque.prepend(10)

print(deque.popFirst()) // Must return 10
print(deque.popLast()) // return 2
