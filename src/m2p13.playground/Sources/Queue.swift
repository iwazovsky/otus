import Foundation

public struct Queue<Element> {
    var elements: LinkedList<Element>

    public init() {
        elements = LinkedList<Element>()
    }
    
    public mutating func enqueue(_ e: Element) {
        elements.prepend(e)
    }
    
    public mutating func dequeue() -> Element? {
        return elements.popFirst()
    }
    
    public func peek() -> Element? {
        return elements.peekFirst()
    }
    
    public func isEmpty() -> Bool {
        return elements.isEmpty()
    }
    
    public func size() -> Int {
        return elements.getSize()
    }
}

