import Foundation

public struct Deque<Element> {
    var elements: LinkedList<Element>
    
    public init() {
        elements = LinkedList<Element>()
    }
    
    public mutating func append(_ e: Element) {
        elements.append(e)
    }
    
    public mutating func prepend(_ e: Element) {
        elements.prepend(e)
    }
    
    public mutating func popFirst() -> Element? {
        return elements.popFirst()
    }
    
    public mutating func popLast() -> Element? {
        return elements.popLast()
    }
    
    public func isEmpty() -> Bool {
        return elements.isEmpty()
    }
}
