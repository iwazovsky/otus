import Foundation

public struct Stack<Element> {
    var elements: LinkedList<Element>
    
    public init() {
        elements = LinkedList<Element>()
    }
    
    public mutating func push(_ e: Element) {
        elements.append(e)
    }
    
    public mutating func pop() -> Element? {
        return elements.popLast()
    }
    
    public func top() -> Element? {
        elements.peekLast()
    }
    
    public func isEmpty() -> Bool {
        return elements.isEmpty()
    }
    
    public func size() -> Int {
        return elements.getSize()
    }
}
