import Foundation


public final class LinkedList<Element> {
    private var first: Node<Element>?
    private var last: Node<Element>?
    
    private var size: Int = 0
    
    public func append(_ e: Element) {
        size += 1
        
        if isEmpty() {
            insertFirst(e)
        } else {
            let n = Node(value: e, prev: last!)
            last!.next = n
            last = n
            
        }
    }
    
    public func prepend(_ e: Element) {
        size += 1
        
        if isEmpty() {
            insertFirst(e)
        } else {
            let n = Node(value: e, next: first!)
            first!.prev = n
            first = n
            
        }
    }
    
    private func insertFirst(_ e: Element) {
        let n = Node(value: e)
        first = n
        last = n
    }

    public func popLast() -> Element? {
        guard let pop = last else {
            return nil
        }
        
        last = pop.prev
        last?.next = nil
        size -= 1
        
        if size == 0 {
            first = nil
            last = nil
        }
        return pop.value
    }
    
    public func popFirst() -> Element? {
        guard let pop = first else {
            return nil
        }
        
        first = pop.next
        first?.prev = nil
        
        
        size -= 1
        
        if size == 0 {
            first = nil
            last = nil
        }
        
        return pop.value
    }
    
    public func peekLast() -> Element? {
        return last?.value
    }

    public func peekFirst() -> Element? {
        return first?.value
    }
    
    public func getSize() -> Int {
        return size
    }
    
    public func isEmpty() -> Bool {
        return first == nil && last == nil
    }
}

public final class Node<Element>: Equatable {
    var id = UUID()
    var value: Element
    
    var prev: Node<Element>?
    var next: Node<Element>?

    init(value: Element, prev: Node?, next: Node?) {
        self.value = value
        self.prev = prev
        self.next = next
    }
    
    convenience init(value: Element, prev: Node) {
        self.init(value: value, prev: prev, next: nil)
    }
    
    convenience init(value: Element, next: Node) {
        self.init(value: value, prev: nil, next: next)
    }
    
    convenience init(value: Element) {
        self.init(value: value, prev: nil, next: nil)
    }
    
    public static func == (lhs: Node<Element>, rhs: Node<Element>) -> Bool {
        return lhs.id == rhs.id
    }
}
