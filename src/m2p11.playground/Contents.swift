/**
 В Playground реализуйте некоторый базовый класс из выбранной вами области описания (тематики).
 Добавьте в него свойства и методы, а также несколько инициализаторов. Класс должен содержать свойства и методы разной области видимости (private, fileprivate, internal или public)
 Создайте несколько классов-наследников. Переопределите при необходимости свойства и методы родителя. А также добавьте свои.
 В основном пространстве Playground создайте функцию для демонстрации полиморфизма.
 К заданию приложите либо архив с Playground, либо ссылку на гитхаб репозиторий, куда загрузите код.
 */

import Foundation



fileprivate enum CourseError: Error {
    case studentIsUnderProtection(String)
    case studentWithoutPremium
}

class OtusCourse {
    var title: String
    var fee: Float
    
    private var students: Set<Student> = []
    private var professors: Set<Professor> = []
    
    required init(title: String, fee: Float) {
        self.title = title
        self.fee = fee
    }
    
    convenience init(title: String, fee: Float, professors: Set<Professor>) {
        self.init(title: title, fee: fee)
        self.professors = professors
    }
    
    func enroll(student: Student) {
        students.insert(student)
    }
    
    func exmatriculate(student: Student) throws {
        if student.underProtection {
            throw CourseError.studentIsUnderProtection("This student is our governor's son")
        }
        
        students.remove(student)
    }
    
    func introduceStudents() {
        for student in students {
            student.introduce()
        }
    }
}


class OtusPremiumPlanCourse: OtusCourse {
    var feeRatio: Float = 1.25
    
    required init(title: String, fee: Float) {
        super.init(title: title, fee: fee * feeRatio)
    }
    
    convenience init(title: String) {
        self.init(title: title, fee: .random(in: 0...10000))
    }

    override func enroll(student: Student) {
        if student.premium {
            super.enroll(student: student)
        }
    }

}


struct Student: Equatable {
    let id = UUID()
    let name: String
    let premium: Bool
    let underProtection: Bool
    
    init(name: String, premium: Bool, underProtection: Bool) {
        self.name = name
        self.premium = premium
        self.underProtection = underProtection
    }
}

extension Student: Hashable {
    static func == (lhs: Student, rhs: Student) -> Bool {
        return lhs.id == rhs.id
    }
}

extension Student {
    func introduce() {
        print(composeInroduction())
    }
    
    private func composeInroduction() -> String {
        return "Hello, my name is \(name) and I am glad to be here"
    }
}

struct Professor: Equatable {
    let id = UUID()
    let name: String
    
    init(name: String) {
        self.name = name
    }
}

extension Professor: Hashable {
    static func ==(lhs: Professor, rhs: Professor) -> Bool {
        return lhs.id == rhs.id
    }
}




var professors: Set<Professor> = [Professor(name: "Dumbledore"), Professor(name: "Snape")]

var student1 = Student(name: "Nickelodeon", premium: true, underProtection: true)
var student2 = Student(name: "Foxer", premium: false, underProtection: false)

var course1 = OtusPremiumPlanCourse(title: "iOS developer", fee: 19)
var course2 = OtusCourse(title: "Android developer", fee: 21)
var course3 = OtusPremiumPlanCourse(title: "Professional barista")
var course4 = OtusCourse(title: "Beginner frontend developer", fee: 10, professors: professors)

var courses: [OtusCourse] = [course1, course2, course3, course4]


//print("Demonstrate premium student applies to premium/basic rouse")
//
//for course in courses {
//    course.enroll(student: student1)
//}
//
//
//course1.introduceStudents()
//course2.introduceStudents()


print("")
print("")

for course in courses {
    course.enroll(student: student2)
}

print("Demonstrate basic student applies to premium/basic rouse")

course1.introduceStudents()
course2.introduceStudents()
