import Foundation

/*
 
1. Исследуйте код ниже и напишите, какие цифры должны вывестись в консоль, обьясните своими словами, почему именно такая последовательность по шагам.
1 9 2 3, после этого случится deadlock, т.к. глобальная очередь (background) передает управление назад главной очереди, которая в свою очередь ждет завершения таска из глобальной background очереди. Программа аварийно завершается.
 
2. Создайте свою сериюную очередь и замените в примере ей DispatchQueue.main, создайте свою конкурентную очередь и заменить ей DispatchQueue.global(qos: .background). Какой будет результат? Всегда ли будет одинаковым И почему?
 
 let serialQueue = DispatchQueue(label: "Own serial queue")
 let concurrentQueue = DispatchQueue(label: "Own concurrent queue", attributes: .concurrent)
 
 print("1")
 serialQueue.async {
     print("2")
     concurrentQueue.sync {
         print("3")
         serialQueue.sync {
             print("4")
             concurrentQueue.async {
                 print("5")
             }
             print("6")
         }
         print("7")
     }
     print("8")
 }
 print("9")
 
 Здесь так же будет deadlock, т.к. выполнение переходит из main queue в серийную, кастомную, очередь, в которую уже пытается вернуться очередь с таском из вызванной ранее очереди
 
3. Какой по номеру надо поменять sync/sync чтобы не возникало дедлока в обоих случаях
 Можно поменять любую из очередь с sync на async и тогда выполнение будет успешным
 Можно так же, например, использовать другую, отличную от двух первых очередь самой первой, "оберточной" и тогда можно использовать 2 sync очереди подряд.
 
4. Как можно сделать в примере, чтобы очередь превратилась из конкурентной в серийную, подправте пример не исправляя создания самой очереди
 
 print("1")
 serialQueue.async {
     print("2")
     concurrentQueue.sync(flags: .barrier) {
         print("3")
         serialQueue.sync {
             print("4")
             concurrentQueue.async(flags: .barrier) {
                 print("5")
             }
             print("6")
         }
         print("7")
     }
     print("8")
 }
 print("9")
 
 
 
 
 */


