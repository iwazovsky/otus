//
//  NetworkConfiguration.swift
//  m2p24
//
//  Created by Konstantin Tukmakov on 27.11.2022.
//

import Foundation

struct AlphavantageNetworkConfiguration {
    
    private let apiUrl = "https://www.alphavantage.co/"
    private let apiKey = "0B9Q16DZYZ0II23I"
    
    func getHeaders() -> [String: String] {
        return [:]
    }
    
    func getBaseUrl() -> String {
        return apiUrl
    }
    
    func getApiKey() -> String {
        return apiKey
    }
}
