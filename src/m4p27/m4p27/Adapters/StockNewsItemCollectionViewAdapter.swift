//
//  StockNewsItemCollectionViewAdapter.swift
//  m2p25
//
//  Created by Konstantin Tukmakov on 11.12.2022.
//

import UIKit

protocol CollectionViewAdapter {
    func setup(_ collectionView: UICollectionView)
}

class StockNewsItemCollectionViewAdapter: NSObject, CollectionViewAdapter {
    var newsItems = [StockNewsItem]()
    
    func setup(_ collectionView: UICollectionView) {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(StockNewsItemViewCell.self, forCellWithReuseIdentifier: "cell")
    }
}

extension StockNewsItemCollectionViewAdapter: UICollectionViewDelegate {
    
}

extension StockNewsItemCollectionViewAdapter: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newsItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? StockNewsItemViewCell else {
            fatalError("Wrong cell is passed to stockNewsItem collectionView")
        }
        cell.item = newsItems[indexPath.item]
        return cell
    }
}

extension StockNewsItemCollectionViewAdapter: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
