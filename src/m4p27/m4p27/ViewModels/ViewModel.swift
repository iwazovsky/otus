//
//  ViewModel.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 08.11.2022.
//

import Foundation

protocol ViewModelDelegate: AnyObject {
    func willLoadData()
    func didLoadData()
}

protocol ViewModel: AnyObject {
    var delegate: ViewModelDelegate? { get set }
    
    func setup()
}
