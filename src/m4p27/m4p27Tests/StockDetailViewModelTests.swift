//
//  m4p27Tests.swift
//  m4p27Tests
//
//  Created by Konstantin Tukmakov on 20.12.2022.
//

import XCTest
@testable import m4p27

final class stockDetailViewModelTests: XCTestCase {
    
    private var stockDetailViewModel  = StockDetailViewModel()
    private var dummyDelegate = ViewModelDelegateDummy()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        stockDetailViewModel.delegate = dummyDelegate
    }

    func testSetup() {
        // given
        let successExpectation = expectation(description: "Success")
        
        stockDetailViewModel.stockService = MockStockService()
        stockDetailViewModel.stockNewsService = MockStockNewsService()
        
        stockDetailViewModel.stock = MockStockService.mockedStock
        
        // when
        self.stockDetailViewModel.setup()
        
        
        // then
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssertNotNil(self.stockDetailViewModel.stockNewsItems)
            XCTAssertEqual(self.stockDetailViewModel.stockNewsItems, MockStockNewsService.mockedUpStockNewsItems)
            
            XCTAssertEqual(self.stockDetailViewModel.stock?.value, 140.105)
            XCTAssertEqual(self.stockDetailViewModel.stockValueFormatted, "140.11 $")
            
            successExpectation.fulfill()
        }
        
        wait(for: [successExpectation], timeout: 1)
        
    }
 
    func testFilterNewsItems() {
        // given
        let successExpectation = expectation(description: "Success")
        stockDetailViewModel.stockNewsService = MockStockNewsService()
        stockDetailViewModel.stock = MockStockService.mockedStock
        
        // when
        self.stockDetailViewModel.filterNewsItems(by: "do")
        
        // then
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssertNotNil(self.stockDetailViewModel.stockNewsItems)
            XCTAssertEqual(self.stockDetailViewModel.stockNewsItems, MockStockNewsService.mockedUpFiltererdStockNewsItems)
            
            successExpectation.fulfill()
        }
        
        wait(for: [successExpectation], timeout: 1)
    }
    
    
}
