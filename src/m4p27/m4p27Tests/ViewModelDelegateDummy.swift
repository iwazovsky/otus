//
//  ViewModelDelegateTest.swift
//  m4p27Tests
//
//  Created by Konstantin Tukmakov on 21.12.2022.
//

import Foundation
@testable import m4p27

class ViewModelDelegateDummy: ViewModelDelegate {
    func willLoadData() {
        
    }
    
    func didLoadData() {
        
    }
}
