//
//  StockNewsServiceMock.swift
//  m4p27Tests
//
//  Created by Konstantin Tukmakov on 21.12.2022.
//

import Foundation
@testable import m4p27

class MockStockNewsService: StockNewsProvider {
    static let mockedUpStockNewsItems = [
        StockNewsItem(symbol: "AAPL", title: "Apple has grown for 10% during 2022", description: "Some description", image: nil),
        StockNewsItem(symbol: "AAPL", title: "Some concerns here in the end of the year", description: "Some another description", image: nil)
    ]
    
    static let mockedUpFiltererdStockNewsItems = [
        StockNewsItem(symbol: "AAPL", title: "Apple does its revolutionary renovation", description: "Some description test", image: nil),
    ]
    
    func fetchStockNewsItems(for stock: m4p27.Stock, completion: @escaping ([m4p27.StockNewsItem]?) -> Void) {
        completion(MockStockNewsService.mockedUpStockNewsItems)
    }
    
    func filterResults(for stock: m4p27.Stock, filterBy: String, completion: @escaping ([m4p27.StockNewsItem]?) -> Void) {
        completion(MockStockNewsService.mockedUpFiltererdStockNewsItems)
    }
    
    
}
