//
//  MockStockService.swift
//  m4p27Tests
//
//  Created by Konstantin Tukmakov on 21.12.2022.
//

import Foundation
@testable import m4p27

class MockStockService: StockProvider {
    
    static let mockedStock: Stock = Stock(symbol: "AAPL", title: "Apple", value: 0.00)
    
    func fetchStocks() -> [m4p27.Stock] {
        return [
            Stock(symbol: "AAPL", title: "Apple"),
            Stock(symbol: "GOOG", title: "Google")
        ]
    }
    
    func fetchPrice(for stock: m4p27.Stock, completion: @escaping ((Double) -> Void)) {
        completion(140.105)
    }
    
}
