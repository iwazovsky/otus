//
//  StockListViewModelTests.swift
//  m4p27Tests
//
//  Created by Konstantin Tukmakov on 20.12.2022.
//

import XCTest
@testable import m4p27

final class StockListViewModelTests: XCTestCase {
    
    var stockListViewModel = StockListViewModel()
    var dummyDelegate = ViewModelDelegateDummy()

    override func setUpWithError() throws {
        stockListViewModel.delegate = dummyDelegate
    }

    func testSetup() {
        // given
        let service = MockStockService()
        stockListViewModel.stockService = service
        
        // when
        stockListViewModel.setup()
        
        // then
//        XCTAssertIdentical(stockListViewModel.stocks, service.fetchStocks())
        XCTAssertNotNil(stockListViewModel.stocks)
        XCTAssertEqual(stockListViewModel.stocks, service.fetchStocks())
    }
}
