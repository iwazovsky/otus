//
//  ConfiguratorsTests.swift
//  m4p27Tests
//
//  Created by Konstantin Tukmakov on 20.12.2022.
//

import XCTest
@testable import m4p27

final class ConfiguratorsTests: XCTestCase {
    
    var stockListConfigurator: StockListConfigurator?
    var stockDetailConfigurator: StockDetailConfigurator?
    
    var testCoordinator: AppCoordinator?
    
    override func setUpWithError() throws {
        continueAfterFailure = false
        
        self.stockListConfigurator = StockListConfigurator()
        self.stockDetailConfigurator = StockDetailConfigurator()
        
        self.testCoordinator = AppCoordinator(navigationController: UINavigationController())
    }
    
    
    func testStockListConfigurator() throws {
        // given
        let viewController = StockListViewController()
        
        // when
        self.stockListConfigurator?.configure(viewController: viewController, coordinator: self.testCoordinator)
        
        
        // then
        XCTAssertNotNil(viewController.viewModel)
        XCTAssertNotNil(viewController.tableViewAdapter)
        
        XCTAssertNotNil(viewController.tableViewAdapter.coordinator)
        
        XCTAssertNotNil(viewController.viewModel.stockService)
        XCTAssertNotNil(viewController.viewModel.delegate)
    }
    
    func testStockDetailConfigurator() throws {
        // given
        let viewController = StockDetailViewController()
        
        // when
        self.stockDetailConfigurator?.configure(viewController: viewController, coordinator: self.testCoordinator)
        
        // then
        XCTAssertNotNil(viewController.viewModel)
        
        XCTAssertNotNil(viewController.viewModel.stockService)
        XCTAssertNotNil(viewController.viewModel.delegate)
        XCTAssertNotNil(viewController.viewModel.stockNewsService)
    }
    
}
