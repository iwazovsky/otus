//
//  Actor.swift
//  OTUSm2p18
//
//  Created by Konstantin Tukmakov on 29.10.2022.
//

import Foundation

struct Actor {
    var name: String
    var titles: [String]
    var images: [ActorImage]
    
    struct ActorImage {
        var path: String
    }
    
    
}

extension Actor {
    static let mockup: [Actor] = {
        return [
            Actor(name: "Neymar", titles: ["Barcelona", "PSG"], images: [Actor.ActorImage(path: "Neymar")]),
            Actor(name: "Leonerdo DiCapricco", titles: ["Titanovich", "Vyzyvshiy"], images: [Actor.ActorImage(path: "DiCaprio")]),
            Actor(name: "Aleksandr Petrov", titles: ["No titles"], images: [Actor.ActorImage(path: "APetrov")])
        ]
    }()
}
