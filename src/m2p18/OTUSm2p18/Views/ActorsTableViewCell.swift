//
//  ActorTableViewCell.swift
//  OTUSm2p18
//
//  Created by Konstantin Tukmakov on 29.10.2022.
//

import UIKit

class ActorsTableViewCell: UITableViewCell {
    
    let portraitImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.layer.cornerRadius = 13
        img.clipsToBounds = true
        return img
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
//        label.textColor
        return label
    }()
    
    var actor: Actor? {
        didSet {
            guard let item = actor else { return }
            nameLabel.text = item.name
            portraitImageView.image = UIImage(named: item.images[0].path)
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews()
        initConstraints()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func initViews() {
        contentView.addSubview(portraitImageView)
        contentView.addSubview(nameLabel)
    }
    
    func initConstraints() {
        portraitImageView.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            portraitImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            portraitImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            portraitImageView.widthAnchor.constraint(equalToConstant: 60),
            portraitImageView.heightAnchor.constraint(equalToConstant: 80),
            
            nameLabel.leadingAnchor.constraint(equalTo: portraitImageView.trailingAnchor, constant: 20),
            nameLabel.centerYAnchor.constraint(equalTo: portraitImageView.centerYAnchor)
            
            
        ])
    }
}
