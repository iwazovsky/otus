//
//  ViewController.swift
//  OTUSm2p18
//
//  Created by Konstantin Tukmakov on 29.10.2022.
//

import UIKit

class MainViewController: UIViewController {
    var actorsTableView: UITableView?
    var actors: [Actor] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initData()
        initViews()
        initConstraints()
        
    }
    
    func initData() {
        actors = Actor.mockup
    }
    
    func initViews() {
        self.view.backgroundColor = .white
    
        actorsTableView = UITableView()
        actorsTableView?.separatorStyle = .none
        actorsTableView?.register(ActorsTableViewCell.self, forCellReuseIdentifier: "cell")
        actorsTableView?.dataSource = self
        actorsTableView?.delegate = self
        
        self.view.addSubview(actorsTableView!)
    }

    func initConstraints() {
        let safeArea = view.safeAreaLayoutGuide
        
        actorsTableView!.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            actorsTableView!.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            actorsTableView!.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            actorsTableView!.topAnchor.constraint(equalTo: safeArea.topAnchor),
            actorsTableView!.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor)
        ])
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailViewController()
        vc.actor = actors[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}


extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ActorsTableViewCell
        
        cell.accessoryType = .disclosureIndicator
        cell.actor = actors[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
}
