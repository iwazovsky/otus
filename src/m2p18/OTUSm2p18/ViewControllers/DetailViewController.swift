//
//  DetailViewController.swift
//  OTUSm2p18
//
//  Created by Konstantin Tukmakov on 29.10.2022.
//

import UIKit

class DetailViewController: UIViewController {
    
    var actor: Actor? {
        didSet {
            guard let actor = actor else { return }
            
            nameLabel.text = actor.name
            titlesLabel.text = "Takes action in: \(actor.titles.joined(separator: ", "))"
            portraitImage.image = UIImage(named: actor.images[0].path)
            
        }
    }
    
    var nameLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 28)
        return label
    }()
    var portraitImage: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFill
        img.layer.cornerRadius = 13
        img.clipsToBounds = true
        return img
    }()
    var titlesLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(actor?.name ?? "")
        
        initViews()
        initConstraints()
    }

    func initViews() {
        self.view.backgroundColor = .white
        
        self.view.addSubview(nameLabel)
        self.view.addSubview(titlesLabel)
        self.view.addSubview(portraitImage)
    }
    
    func initConstraints() {
        let safeArea = self.view.safeAreaLayoutGuide
    
        NSLayoutConstraint.activate([
            portraitImage.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 40),
            portraitImage.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            portraitImage.widthAnchor.constraint(equalToConstant: 100),
            portraitImage.heightAnchor.constraint(equalToConstant: 120),
            
            nameLabel.topAnchor.constraint(equalTo: portraitImage.bottomAnchor, constant: 20),
            nameLabel.centerXAnchor.constraint(equalTo: portraitImage.centerXAnchor),
            
            titlesLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 40),
            titlesLabel.centerXAnchor.constraint(equalTo: nameLabel.centerXAnchor)
        ])
    }
}
