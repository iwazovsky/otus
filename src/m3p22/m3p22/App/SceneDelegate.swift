//
//  SceneDelegate.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 06.11.2022.
//

import UIKit


class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var coordinator: ICoordinator?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let navigationController = UINavigationController()
        navigationController.pushViewController(RootViewController(), animated: false)
        navigationController.pushViewController(OnboardingViewController(), animated: true)
        window = UIWindow(windowScene: windowScene)
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        
//        coordinator = AppCoordinator(navigationController: navigationController)
//        coordinator?.start()
//
//        window?.rootViewController = navigationController
//        window?.makeKeyAndVisible()
    }


}

