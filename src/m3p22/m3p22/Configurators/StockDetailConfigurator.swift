//
//  StockDetailConfigurator.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 12.11.2022.
//

import UIKit

class StockDetailConfigurator: IConfigurator {
    
    func configure(viewController: UIViewController, coordinator: ICoordinator?) {
        guard let viewController = viewController as? StockDetailViewController else { return }
        
        let viewModel = StockDetailViewModel()
        viewModel.stockService = StockService.shared
        viewModel.stockNewsService = StockNewsService.shared
        
        
        viewModel.delegate = viewController
        viewController.viewModel = viewModel
        viewController.stockNewsItemCollectionViewAdapter = StockNewsItemCollectionViewAdapter()
    }
    
}
