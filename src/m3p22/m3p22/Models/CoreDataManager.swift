//
//  File.swift
//  m2p25
//
//  Created by Konstantin Tukmakov on 30.11.2022.
//

import Foundation
import CoreData

enum DatabaseError: Error {
    case error
}

class CoreDataManager {
    static let shared = CoreDataManager(modelName: "StockDataModel")
    
    var modelName: String
    
    private lazy var mainContext: NSManagedObjectContext = {
        return self.persistentContainer.viewContext
    }()
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: self.modelName)
        container.loadPersistentStores { description, error in
            if let error = error {
                fatalError("Unable to load persistent stores: \(error)")
            }
        }
        return container
    }()
    
    init(modelName: String) {
        self.modelName = modelName
    }
    
    func createStockNewsItem(_ stockNewsItem: StockNewsItem, completion: @escaping(Result<StockNewsItemModel, DatabaseError>) -> Void) {
        let model = StockNewsItemModel(context: self.mainContext)
        
        model.symbol = stockNewsItem.symbol
        model.content = stockNewsItem.description
        model.title = stockNewsItem.title
        model.image = stockNewsItem.image
        
        guard mainContext.hasChanges else {
            completion(.failure(.error))
            return
        }
        
        do {
            try mainContext.save()
            completion(.success(model))
        } catch {
            completion(.failure(.error))
        }
    }
    
    func getStockNewsItems(predicate: NSPredicate?, completion: @escaping(Result<[StockNewsItemModel], DatabaseError>) -> Void) {
        let fetchRequest = StockNewsItemModel.fetchRequest()
        fetchRequest.predicate = predicate
        
        guard let result = try? mainContext.fetch(fetchRequest) else {
            completion(.failure(.error))
            return
        }
        
        completion(.success(result))
    }
}
