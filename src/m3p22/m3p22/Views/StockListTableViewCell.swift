//
//  StockListTableViewCell.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 07.11.2022.
//

import UIKit.UITableViewCell

class StockListTableViewCell: UITableViewCell {
    var stock: Stock? {
        didSet {
            guard let stock = stock else { return }
            titleLabel.text = stock.title
        }
    }
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    required init?(coder: NSCoder) {
        fatalError("Storyboards are not supported")
    }
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews()
        initConstraints()
    }
    
    func initViews() {
        contentView.addSubview(titleLabel)
    }
    
    func initConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
}
