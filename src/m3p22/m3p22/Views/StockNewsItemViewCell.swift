//
//  StockNewsItemViewCell.swift
//  m2p25
//
//  Created by Konstantin Tukmakov on 11.12.2022.
//

import UIKit

class StockNewsItemViewCell: UICollectionViewCell {
    
    var item: StockNewsItem? {
        didSet {
            guard let item = item else { return }
            titleLabel.text = item.title
            descriptionLabel.text = item.description

            if let imageData = item.image {
                image.image = UIImage(data: imageData)
            }
        }
    }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var image: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleToFill
        image.layer.cornerRadius = 13
        image.clipsToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
        initConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Storyboards not supported")
    }
    
    func initViews() {
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(image)
    }
    
    func initConstraints() {
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            image.heightAnchor.constraint(equalToConstant: 130),
            image.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 5),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
        ])
    }
}
