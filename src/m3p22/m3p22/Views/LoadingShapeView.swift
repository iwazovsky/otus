//
//  LoadingShapeView.swift
//  m3p22
//
//  Created by Konstantin Tukmakov on 22.12.2022.
//

import UIKit

final class LoadingShapeView: UIView {
    
    var bgColor: UIColor?
    var strokeColor: UIColor?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        
        let fillColor = bgColor ?? .white
        let strokeColor = strokeColor ?? .black
        
        fillColor.setFill()
        UIRectFill(rect)
        
        let path = getPath(rect)
        path.lineWidth = 3
       
        strokeColor.setStroke()
        path.stroke()
       
    }

    required init(coder aCoder: NSCoder) {
        fatalError("Storyboards are not supported")
    }
    
    func getPath(_ rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()
        let maxX = rect.width
        let maxY = rect.height

        let centerPoint = CGPoint(x: maxX/2, y: maxY/2)
        
        path.move(to: centerPoint)

        path.addLine(to: CGPoint(x: maxX/2, y: 0))
        path.addLine(to: centerPoint)

        path.addLine(to: CGPoint(x: maxX, y: maxY/2))
        path.addLine(to: centerPoint)

        path.addLine(to: CGPoint(x: maxX/2, y: maxY))
        path.addLine(to: centerPoint)

        path.addLine(to: CGPoint(x: 0, y: maxY/2))
        path.addLine(to: centerPoint)
//
        return path
    }
}
