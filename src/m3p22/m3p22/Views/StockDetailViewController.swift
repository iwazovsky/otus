//
//  StockDetailViewController.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 08.11.m2022.
//

import UIKit

class StockDetailViewController: UIViewController {
    
    var stockNewsItemCollectionViewAdapter: StockNewsItemCollectionViewAdapter!
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    lazy var newsSectionTitle: UILabel = {
        let title = UILabel()
        title.text = "Latest News"
        title.font = .boldSystemFont(ofSize: 20)
        title.translatesAutoresizingMaskIntoConstraints = false
        return title
    }()
    
    lazy var searchInNewsInput: UITextField = {
        let field = UITextField()
        field.placeholder = "Search"
        field.addTarget(self, action: #selector(onUserInput), for: .editingChanged)
        field.translatesAutoresizingMaskIntoConstraints = false
        return field
    }()
    
    lazy var newsSectionCollectionView: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: self.newsSectionCollectionViewLayout)
        collection.showsVerticalScrollIndicator = true
        collection.translatesAutoresizingMaskIntoConstraints = false
        stockNewsItemCollectionViewAdapter.setup(collection)
        return collection
    }()
    
    lazy var newsSectionCollectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 25
        return layout
    }()
    
    private lazy var loadingIcon: LoadingShapeView = {
        let view = LoadingShapeView()
        view.bgColor = .white
        view.strokeColor = .black
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var viewModel: StockDetailViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        viewModel.setup()
        
        initViews()
        initConstraints()
        
    }
    
    private func initViews() {
        view.addSubview(titleLabel)
        view.addSubview(valueLabel)
        
        view.addSubview(loadingIcon)
        
        view.addSubview(newsSectionTitle)
        view.addSubview(searchInNewsInput)
        view.addSubview(newsSectionCollectionView)
        
        addLoadingAnimation()
        
    }
    
    func addLoadingAnimation() {
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = CGFloat(Double.pi * 2)
        rotationAnimation.isRemovedOnCompletion = false
        rotationAnimation.duration = 0.4
        rotationAnimation.repeatCount = Float.infinity
        
        loadingIcon.layer.add(rotationAnimation, forKey: nil)
    }
    
    private func initConstraints() {
        let safeArea = view.safeAreaLayoutGuide
        
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            titleLabel.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 20),
            valueLabel.centerXAnchor.constraint(equalTo: titleLabel.centerXAnchor),
            valueLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 2),
            
            newsSectionTitle.topAnchor.constraint(equalTo: valueLabel.bottomAnchor, constant: 20),
            newsSectionTitle.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 20),

            loadingIcon.topAnchor.constraint(equalTo: newsSectionTitle.bottomAnchor, constant: 20),
            loadingIcon.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            loadingIcon.widthAnchor.constraint(equalToConstant: 50),
            loadingIcon.heightAnchor.constraint(equalToConstant: 50),
            
            searchInNewsInput.topAnchor.constraint(equalTo: loadingIcon.bottomAnchor, constant: 5),
            searchInNewsInput.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 20),
            searchInNewsInput.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -20),
            
            newsSectionCollectionView.topAnchor.constraint(equalTo: searchInNewsInput.bottomAnchor, constant: 10),
            newsSectionCollectionView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor),
            newsSectionCollectionView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 20),
            newsSectionCollectionView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -20),
        ])
    }

    @objc func onUserInput() {
        viewModel.filterNewsItems(by: searchInNewsInput.text ?? "")
    }
}

extension StockDetailViewController: ViewModelDelegate {
    func willLoadData() {
        
    }
    
    func didLoadData() {
        
        if let stock = viewModel.stock {
            titleLabel.text = "\(stock.title) (\(stock.symbol.uppercased()))"
            let stockValue = viewModel.stock?.value ?? 0
            valueLabel.text = viewModel.stockValueFormatted
        }

        if let items = viewModel.stockNewsItems {
            loadingIcon.removeFromSuperview()
            
            
            searchInNewsInput.topAnchor.constraint(equalTo: newsSectionTitle.bottomAnchor, constant: 5).isActive = true
            
            stockNewsItemCollectionViewAdapter.newsItems = items
            newsSectionCollectionView.reloadData()
        }
        
        
        
        
    }
}
