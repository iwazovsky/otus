//
//  OnboardingViewController.swift
//  m3p22
//
//  Created by Konstantin Tukmakov on 22.12.2022.
//

import UIKit

final class OnboardingViewController: UIViewController {
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var onboardingPageViewController: OnboardingPageViewController = {
        let view = OnboardingPageViewController()
        view.pages = [
            OnboardingStockListViewController(),
            OnboardingStockDetailViewController()
        ]
        view.view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        initConstraints()
        
        
        onboardingPageViewController.didMove(toParent: self)
    }
    
    func initViews() {
        addChild(onboardingPageViewController)
        
        view.addSubview(containerView)
        containerView.addSubview(onboardingPageViewController.view)
    }
    
    func initConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: view.topAnchor),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            
            onboardingPageViewController.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            onboardingPageViewController.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            onboardingPageViewController.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            onboardingPageViewController.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
        ])
    }
}
