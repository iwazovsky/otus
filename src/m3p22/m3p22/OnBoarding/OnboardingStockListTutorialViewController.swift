//
//  OnboardingStockListTutorialViewController.swift
//  m3p22
//
//  Created by Konstantin Tukmakov on 22.12.2022.
//

import UIKit

final class OnboardingStockListViewController: UIViewController {
    
    private lazy var tutorialImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "onboarding_listView")
        image.clipsToBounds = true
        image.contentMode = .scaleToFill
        image.translatesAutoresizingMaskIntoConstraints = false
        image.alpha = 0
        return image
    }()
    
    private lazy var firstInstruction: UILabel = {
        let label = UILabel()
        label.text = "1. This is your stocks"
        label.font = .systemFont(ofSize: 24)
        label.textColor = .darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.alpha = 0
        return label
    }()
    
    private lazy var secondInstruction: UILabel = {
        let label = UILabel()
        label.text = "2. Click on one in the list to see details"
        label.font = .systemFont(ofSize: 24)
        label.textColor = .darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.alpha = 0
        return label
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        initViews()
        initConstaints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        UIView.animateKeyframes(withDuration: 8, delay: 0, options: [], animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.25) {
                self.tutorialImage.alpha = 1
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.375, relativeDuration: 0.25) {
                self.firstInstruction.alpha = 1
            }

            UIView.addKeyframe(withRelativeStartTime: 0.75, relativeDuration: 0.25) {
                self.secondInstruction.alpha = 1
            }
        })
    }

    func initViews() {
        view.addSubview(tutorialImage)
        view.addSubview(firstInstruction)
        view.addSubview(secondInstruction)
    }
    
    func initConstaints() {
        let safeArea = view.safeAreaLayoutGuide
        
        NSLayoutConstraint.activate([
            tutorialImage.topAnchor.constraint(equalTo: safeArea.topAnchor),
            tutorialImage.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            tutorialImage.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            tutorialImage.heightAnchor.constraint(equalToConstant: 300),
            
            firstInstruction.topAnchor.constraint(equalTo: tutorialImage.bottomAnchor, constant: 60),
            firstInstruction.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            
            secondInstruction.topAnchor.constraint(equalTo: firstInstruction.bottomAnchor, constant: 10),
            secondInstruction.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor)
        ])
    }
}
