//
//  OnboardingStockDetailViewController.swift
//  m3p22
//
//  Created by Konstantin Tukmakov on 22.12.2022.
//

import UIKit

final class OnboardingStockDetailViewController: UIViewController {
    
    private lazy var tutorialImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "onboarding_detailView")
        image.clipsToBounds = true
        image.contentMode = .scaleToFill
        image.translatesAutoresizingMaskIntoConstraints = false
        image.alpha = 0
        return image
    }()
    
    private lazy var firstInstruction: UILabel = {
        let label = UILabel()
        label.text = "1. Here you can see current price"
        label.font = .systemFont(ofSize: 24)
        label.textColor = .darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.alpha = 0
        return label
    }()
    
    private lazy var secondInstruction: UILabel = {
        let label = UILabel()
        label.text = "And latest news"
        label.font = .systemFont(ofSize: 24)
        label.textColor = .darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.alpha = 0
        return label
    }()
    
    private lazy var closeTutorialButton: UIButton = {
        var configuration = UIButton.Configuration.filled()
        configuration.title = "Go to app"
        configuration.buttonSize = .large
        
        let button = UIButton(configuration: configuration)

        button.translatesAutoresizingMaskIntoConstraints = false
        button.alpha = 0
        
        button.addTarget(self, action: #selector(onCloseTutorialButtonClick), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        initViews()
        initConstaints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        UIView.animateKeyframes(withDuration: 11, delay: 0, options: [], animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.182) {
                self.tutorialImage.alpha = 1
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.273, relativeDuration: 0.182) {
                self.firstInstruction.alpha = 1
            }

            UIView.addKeyframe(withRelativeStartTime: 0.545, relativeDuration: 0.182) {
                self.secondInstruction.alpha = 1
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.818, relativeDuration: 0.182) {
                self.closeTutorialButton.alpha = 1
            }
        })
    }
    
    func initViews() {
        view.addSubview(tutorialImage)
        view.addSubview(firstInstruction)
        view.addSubview(secondInstruction)
        view.addSubview(closeTutorialButton)
    }
    
    func initConstaints() {
        let safeArea = view.safeAreaLayoutGuide
        
        NSLayoutConstraint.activate([
            tutorialImage.topAnchor.constraint(equalTo: safeArea.topAnchor),
            tutorialImage.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            tutorialImage.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            tutorialImage.heightAnchor.constraint(equalToConstant: 380),
            
            firstInstruction.topAnchor.constraint(equalTo: tutorialImage.bottomAnchor, constant: 60),
            firstInstruction.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            
            secondInstruction.topAnchor.constraint(equalTo: firstInstruction.bottomAnchor, constant: 10),
            secondInstruction.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            
            closeTutorialButton.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: 15),
            closeTutorialButton.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor)
        ])
    }
    
    @objc func onCloseTutorialButtonClick() {
        guard let navigationController = navigationController else {
            return
        }
        
        navigationController.popToRootViewController(animated: false)
        
        let coordinator = AppCoordinator(navigationController: navigationController)
        coordinator.start()
    }
}
