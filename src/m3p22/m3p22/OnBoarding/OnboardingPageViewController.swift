//
//  OnBoardingView.swift
//  m3p22
//
//  Created by Konstantin Tukmakov on 22.12.2022.
//

import UIKit

final class OnboardingPageViewController: UIPageViewController {
    
    var pages: [UIViewController]!
    
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Storyboards are not supported")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let pc = UIPageControl.appearance()
        pc.pageIndicatorTintColor = .lightGray
        pc.currentPageIndicatorTintColor = .black
        pc.backgroundColor = .white
        
        self.dataSource = self
        self.delegate = self
        
        
        setViewControllers([pages[0]], direction: .forward, animated: false)
    }
    
    
}

extension OnboardingPageViewController: UIPageViewControllerDelegate {
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {

        guard let firstViewController = pageViewController.viewControllers?.first else {
            return 0
        }
        guard let firstViewControllerIndex = pages.firstIndex(of: firstViewController) else {
            return 0
        }

        return firstViewControllerIndex
    }
}

extension OnboardingPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.firstIndex(of: viewController) else {
            print("nil in here")
            return nil
            
        }
        

        guard currentIndex - 1 >= 0 else {
            return pages.last
        }
        
        return pages[currentIndex - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let currentIndex = pages.firstIndex(of: viewController) else {
            print("or nil in here")
            return nil
            
        }
        
        guard currentIndex + 1 < pages.count else {
            return pages.first
        }
        
        return pages[currentIndex + 1]
    }
    
    
}
