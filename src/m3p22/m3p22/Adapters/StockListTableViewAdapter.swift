//
//  File.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 06.11.2022.
//

import UIKit

protocol TableViewAdapter {
    func setup(_ tableView: UITableView)
}

protocol StockListViewTableAdapterCoordinator {
    func showStock(_ stock: Stock)
}

final class StockListTableViewAdapter: NSObject, TableViewAdapter {
    
    var stocks: [Stock] = []
    var coordinator: StockListViewTableAdapterCoordinator?
    
    func setup(_ tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(StockListTableViewCell.self, forCellReuseIdentifier: String(describing: StockListTableViewCell.self))
    }
    
}


// MARK: – UITableViewDelegate
extension StockListTableViewAdapter: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        coordinator?.showStock(stocks[indexPath.row])
    }
}

// MARK: – UITableViewDataSource
extension StockListTableViewAdapter: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stocks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: StockListTableViewCell.self), for: indexPath) as! StockListTableViewCell
        cell.stock = stocks[indexPath.row]
        return cell
    }
}

