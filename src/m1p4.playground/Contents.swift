import XCTest



func firstRepeated(_ numbers: [Int]) -> Int {
    for i in 0..<numbers.count {
        for k in i+1..<numbers.count {
            if numbers[i] == numbers[k] {
                return i
            }
        }
    }
    
    return -1
}


class SolutionTests: XCTestCase {
    
    func testBasic() {
        XCTAssertEqual(firstRepeated([1, 3, 2, 4, 3]), 1)
        XCTAssertEqual(firstRepeated([12, 44, 0, 1, 43, 21, 340, 1]), 3)
    }
    
    func testHasNoDuplicates() {
        XCTAssertEqual(firstRepeated([101, 32, 232, 22, 10]), -1)
    }
    
    func testRandom() {
        let random = generateRandom()
        var result = -1
        
        for i in 0..<random.count {
            for k in i+1..<random.count {
                if random[i] == random[k] {
                    print(i)
                    result = i
                    break
                }
            }
        }

        XCTAssertEqual(firstRepeated(random), result)
    }
    
    func generateRandom() -> [Int] {
        // generate random number for our array size
        let size = Int.random(in: 1...10)
        // hydrate array with random numbers
        let random: [Int] = (0...size).map { _ in Int.random(in: 0...100) }
        return random
    }
}

SolutionTests.defaultTestSuite.run()

