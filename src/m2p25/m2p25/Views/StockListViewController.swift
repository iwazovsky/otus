//
//  StockListViewController.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 06.11.2022.
//

import UIKit

protocol IStockListCoordinator {
    
}

final class StockListViewController: UIViewController {
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableViewAdapter.setup(tableView)
        return tableView
    }()
    
    var viewModel: StockListViewModel!
    var tableViewAdapter: StockListTableViewAdapter!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        viewModel.setup()
        
        initViews()
        initConstraints()
    }

    private func initViews() {
        view.addSubview(tableView)
    }
    
    private func initConstraints() {
        let safeArea = view.safeAreaLayoutGuide
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeArea.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor)
        ])
    }
}

extension StockListViewController: ViewModelDelegate {
    func willLoadData() {
        
    }
    
    func didLoadData() {
        tableViewAdapter.stocks = viewModel.stocks
    }
}
