//
//  JsonHelper.swift
//  m2p24
//
//  Created by Konstantin Tukmakov on 27.11.2022.
//

import Foundation

class JsonHelper {
    static let shared = JsonHelper()
    private var decoder = JSONDecoder()
    
    func decode<T: Decodable>(data: Data, _ type: T.Type) -> T? {
//        decoder.keyDecodingStrategy = .convertFromSnakeCase
        var item: T? = nil
        
        do {
            item = try decoder.decode(type, from: data)
        } catch {
            print(error)
        }
        return item
    }
}
