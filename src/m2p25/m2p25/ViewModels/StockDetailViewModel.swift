//
//  StockDetailViewModel.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 08.11.2022.
//

import Foundation

class StockDetailViewModel: ViewModel {
    weak var delegate: ViewModelDelegate?
    var stock: Stock?
    var stockNewsItems: [StockNewsItem]?
    var stockService: StockService!
    var stockNewsService: StockNewsProvider!
    
    func setup() {
        
        DispatchQueue.main.async {
            guard self.stock != nil else { return }
            
            self.stockService.fetchPrice(for: self.stock!) { price in
                self.stock?.value = price
                self.delegate?.didLoadData()
            }
        }
        
        DispatchQueue.main.async {
            guard self.stock != nil else { return }
            
            self.stockNewsService.fetchStockNewsItems(for: self.stock!) { newsItems in
                self.stockNewsItems = newsItems
                self.delegate?.didLoadData()
            }
        }
        
        delegate?.didLoadData()
    }
    
    func filterNewsItems(by userInput: String) {
        DispatchQueue.main.async {
            guard self.stock != nil else { return }
            
            self.stockNewsService.filterResults(for: self.stock!, filterBy: userInput) { newsItems in
                self.stockNewsItems = newsItems
                self.delegate?.didLoadData()
            }
        }
    }
}

