//
//  StockNewsItem.swift
//  m2p25
//
//  Created by Konstantin Tukmakov on 11.12.2022.
//

import Foundation

struct StockNewsItem: Decodable {
    
    let symbol: String
    let title: String
    let description: String
    var date: Date?
    let image: Data?
    
}
