//
//  SceneDelegate.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 06.11.2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        window = UIWindow(windowScene: windowScene)
        
        appCoordinator = AppCoordinator(navigationController: UINavigationController())
        appCoordinator?.start()
        
        window?.rootViewController = appCoordinator?.navigationController
        window?.makeKeyAndVisible()
    }


}

