//
//  Configurator.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 12.11.2022.
//

import UIKit

class StockListConfigurator: IConfigurator {
    func configure(viewController: UIViewController, coordinator: ICoordinator) {
        
        guard let viewController = viewController as? StockListViewController else { return }
        guard let coordinator = coordinator as? AppCoordinator else { return }
        
        viewController.coordinator = coordinator
        
        let viewModel = StockListViewModel()
        viewModel.delegate = viewController
        viewController.viewModel = viewModel
        
        let tableViewAdapter = StockListTableViewAdapter()
        tableViewAdapter.coodrinator = coordinator
        viewController.tableViewAdapter = tableViewAdapter
    }
}
