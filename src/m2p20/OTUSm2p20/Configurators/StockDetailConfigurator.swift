//
//  StockDetailConfigurator.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 12.11.2022.
//

import UIKit

class StockDetailConfigurator: IConfigurator {
    
    func configure(viewController: UIViewController, coordinator: ICoordinator) {
        guard let viewController = viewController as? StockDetailViewController else { return }
        guard let coordinator = coordinator as? AppCoordinator else { return }
        
        viewController.coordinator = coordinator
        
        let viewModel = StockDetailViewModel()
        viewModel.delegate = viewController
        viewController.viewModel = viewModel
    }
    
}
