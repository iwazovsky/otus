//
//  StockListViewModel.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 06.11.2022.
//

import Foundation
import UIKit.UITableView


final class StockListViewModel: ViewModel {
    weak var delegate: ViewModelDelegate?
    var stocks: [Stock] = []
    
    func setup() {
        stocks = fetchStocks()
        delegate?.didLoadData()
    }
    
    func fetchStocks() -> [Stock] {
        return [
            Stock(title: "Apple", value: 23.32),
            Stock(title: "Google", value: 5.44),
            Stock(title: "Amazon", value: 324.21)
        ]
    }
    
}
