//
//  StockDetailViewModel.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 08.11.2022.
//

import Foundation

class StockDetailViewModel: ViewModel {
    weak var delegate: ViewModelDelegate?
    var stock: Stock?
    
    func setup() {
        delegate?.didLoadData()
    }
}
