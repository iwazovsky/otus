//
//  AppCoordinator.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 09.11.2022.
//

import UIKit

protocol ICoordinator {
    var children: [ICoordinator] { get }
    var navigationController: UINavigationController? { get set }
    
    func start()
}

final class AppCoordinator: ICoordinator {
    private(set) var children: [ICoordinator] = []
    var navigationController: UINavigationController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = StockListViewController()
        let configurator = StockListConfigurator()
        configurator.configure(viewController: vc, coordinator: self)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func showStock(_ stock: Stock) {
        let vc = StockDetailViewController()
        let configurator = StockDetailConfigurator()
        configurator.configure(viewController: vc, coordinator: self)
        
        vc.viewModel.stock = stock
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
