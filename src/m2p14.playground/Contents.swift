
/**
 Напишите несколько примеров и функций:

 Функция, принимающая функцию как параметр
 Функция с несколькими замыканиями
 Функция с autoclosure
 Использование внутренних функций
 Дженерик-функция с условием
 В Playground добавьте пример для демонстрации работы.
 */

func doSomeTask(_ completion: () -> Void) {
    print("Doing task in func")
    completion()
    print("After completion")
}

func processSalary(salary: Float, pre: (Float) -> (Float), saveHandler: @escaping (Float) -> Void) -> Float {
    func includeTaxes(_ a: Float) -> Float {
        return (a * 0.75) - 200
    }


    var processed = includeTaxes(pre(salary))
    print("\(processed)")
    saveHandler(processed)

    return processed
}

func doubleIt<T>(_ number: T) -> T where T: Numeric {
    return number * number
}

func saveSalary(_ salary: Float) {
    print("Do network request to save user's salary")
}


func something(_ log: @autoclosure () -> Void) {
    print("Do something")
    print("If log is enabled")
    if true {
        log()
    }
    print("And maybe do something more")
}

func log(_ msg: String) {
    print(msg)
}



//doSomeTask { [weak self] in
//    print("From completion function")
//}

//processSalary(salary: 100.0, pre: doubleIt, saveHandler: saveSalary)

//something(log("Test run successfully"))
