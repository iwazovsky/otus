//
//  Stock.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 06.11.2022.
//

import Foundation

struct Stock {
    var title: String
    var value: Double
}
