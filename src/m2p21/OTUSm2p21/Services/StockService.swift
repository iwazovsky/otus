//
//  StockService.swift
//  OTUSm2p21
//
//  Created by Konstantin Tukmakov on 12.11.2022.
//

import Foundation

class StockService {
    static let shared = StockService()
    
    private init() {}
    
    func fetchStocks() -> [Stock] {
        return [
            Stock(title: "Apple", value: 23.32),
            Stock(title: "Google", value: 5.44),
            Stock(title: "Amazon", value: 324.21)
        ]
    }
}
