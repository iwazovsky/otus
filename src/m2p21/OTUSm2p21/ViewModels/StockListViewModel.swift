//
//  StockListViewModel.swift
//  OTUSm2p20
//
//  Created by Konstantin Tukmakov on 06.11.2022.
//

import UIKit


final class StockListViewModel: ViewModel {
    weak var delegate: ViewModelDelegate?
    var stockService: StockService!
    var stocks: [Stock] = []
    
    func setup() {
        stocks = stockService.fetchStocks()
        delegate?.didLoadData()
    }
    
    
}
